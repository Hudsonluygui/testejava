package testejava;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

//Desenvolvido por Hudson Luygui


public class TesteJava {

    String[] F_Massa = new String[10];
    String[] R_BARRICHELLO = new String[10];
    String[] K_RAIKKONEN = new String[10];
    String[] M_WEBBER = new String[10];
    String[] F_ALONSO = new String[10];
    String[] S_VETTEL = new String[10];
    String[] Final = new String[10];

    public static void main(String[] args) {
        TesteJava Construtor = new TesteJava();

    }

    public TesteJava() {
        IniciarVetores();

    }

    public void IniciarVetores() {
        F_Massa[0] = "1:02.852";
        F_Massa[1] = "1:03.170";
        F_Massa[2] = "1:02.769";
        F_Massa[3] = "1:02.787";
        F_Massa[4] = "F.Massa";
        F_Massa[5] = "038";
        F_Massa[6] = Converter(F_Massa);

        R_BARRICHELLO[0] = "1:04.352";
        R_BARRICHELLO[1] = "1:04.002";
        R_BARRICHELLO[2] = "1:03.716";
        R_BARRICHELLO[3] = "1:04.010";
        R_BARRICHELLO[4] = "R.BARRICHELLO";
        R_BARRICHELLO[5] = "033";
        R_BARRICHELLO[6] = Converter(R_BARRICHELLO);

        K_RAIKKONEN[0] = "1:04.108";
        K_RAIKKONEN[1] = "1:03.982";
        K_RAIKKONEN[2] = "1:03.987";
        K_RAIKKONEN[3] = "1:03.076";
        K_RAIKKONEN[4] = "K.RAIKKONEN";
        K_RAIKKONEN[5] = "002";
        K_RAIKKONEN[6] = Converter(K_RAIKKONEN);

        M_WEBBER[0] = "1:04.414";
        M_WEBBER[1] = "1:03.982";
        M_WEBBER[2] = "1:04.287";
        M_WEBBER[3] = "1:04.216";
        M_WEBBER[4] = "M.WEBBER";
        M_WEBBER[5] = "023";
        M_WEBBER[6] = Converter(M_WEBBER);

        F_ALONSO[0] = "1:18.456";
        F_ALONSO[1] = "1:07.011";
        F_ALONSO[2] = "1:08.704";
        F_ALONSO[3] = "1:20.050";
        F_ALONSO[4] = "F.ALONSO";
        F_ALONSO[5] = "015";
        F_ALONSO[6] = Converter(F_ALONSO);

        S_VETTEL[0] = "3:31.315";
        S_VETTEL[1] = "1:37.864";
        S_VETTEL[2] = "1:18.097";
        S_VETTEL[3] = null;
        S_VETTEL[4] = "S.VETTEL";
        S_VETTEL[5] = "011";
        S_VETTEL[6] = Converter(S_VETTEL);

        Final();
    }

    public String Converter(String[] Pilotos) {
        Double Convert = null;
        Double Resultado = null;
        for (int X = 0; X <= 3; X++) {
            if ((Pilotos[X]) == null) {
                Pilotos[X] = "0:00.000";
            }
            String Minutos = Pilotos[X].substring(0, 1);
            Convert = Double.parseDouble(Minutos);
            Convert = Convert / 60; //Converter em Segundos
            Convert = Convert / 1000; //Converter em Milessimos

            String Segundos = Pilotos[X].substring(2, 4);
            Convert = Convert + (Double.parseDouble(Segundos) / 1000);

            String Milissegundos = Pilotos[X].substring(5, 8);
            Convert = Convert + Double.parseDouble(Milissegundos);
            if (X == 0) {
                Resultado = Convert;
            } else {
                Resultado = Resultado + Convert;
            }
        }
        return String.valueOf(Resultado);
    }

    public void Final() {
        String Aux;
        int X;
        int Y;
        int QT_Voltas = 0;
        Final[0] = F_Massa[6];
        Final[1] = R_BARRICHELLO[6];
        Final[2] = K_RAIKKONEN[6];
        Final[3] = M_WEBBER[6];
        Final[4] = F_ALONSO[6];
        Final[5] = S_VETTEL[6];
        for (X = 0; X < 6; X++) {
            for (Y = 0; Y < 5; Y++) {
                if ((Double.parseDouble(Final[Y])) > Double.parseDouble(Final[Y + 1])) {
                    Aux = Final[Y];
                    Final[Y] = Final[Y + 1];
                    Final[Y + 1] = Aux;
                }
            }
        }
        for (X = 0; X < 6; X++) {
            if (Double.parseDouble(Final[X]) == Double.parseDouble(F_Massa[6])) {
                F_Massa[7] = X + 1 + "º Lugar";
                F_Massa[9] = X + 1 + "";
            } else if (Double.parseDouble(Final[X]) == Double.parseDouble(R_BARRICHELLO[6])) {
                R_BARRICHELLO[7] = X + 1 + "º Lugar";
                R_BARRICHELLO[9] = X + 1 + "";

            } else if (Double.parseDouble(Final[X]) == Double.parseDouble(K_RAIKKONEN[6])) {
                K_RAIKKONEN[7] = X + 1 + "º Lugar";
                K_RAIKKONEN[9] = X + 1 + "";
            } else if (Double.parseDouble(Final[X]) == Double.parseDouble(M_WEBBER[6])) {
                M_WEBBER[7] = X + 1 + "º Lugar";
                M_WEBBER[9] = X + 1 + "";
            } else if (Double.parseDouble(Final[X]) == Double.parseDouble(F_ALONSO[6])) {
                F_ALONSO[7] = X + 1 + "º Lugar";
                F_ALONSO[9] = X + 1 + "";
            } else if (Double.parseDouble(Final[X]) == Double.parseDouble(S_VETTEL[6])) {
                S_VETTEL[7] = X + 1 + "º Lugar";
                S_VETTEL[9] = X + 1 + "";
            }
        }
        X = 0;
        while (X != 4) {
            if (!F_Massa[X].equals("0:00.000")) {
                QT_Voltas++;
                F_Massa[8] = String.valueOf(QT_Voltas);
            }
            X++;
        }
        QT_Voltas = 0;
        X = 0;
        while ((X != 4)) {
            if (!R_BARRICHELLO[X].equals("0:00.000")) {
                QT_Voltas++;
                R_BARRICHELLO[8] = String.valueOf(QT_Voltas);
            }
            X++;
        }
        X = 0;
        QT_Voltas = 0;
        while (X != 4) {
            if (!K_RAIKKONEN[X].equals("0:00.000")) {
                QT_Voltas++;
                K_RAIKKONEN[8] = String.valueOf(QT_Voltas);
            }
            X++;
        }
        X = 0;
        QT_Voltas = 0;
        while (X != 4) {
            if (!M_WEBBER[X].equals("0:00.000")) {
                QT_Voltas++;
                M_WEBBER[8] = String.valueOf(QT_Voltas);
            }
            X++;
        }
        X = 0;
        QT_Voltas = 0;
        while (X != 4) {
            if (!F_ALONSO[X].equals("0:00.000")) {
                QT_Voltas++;
                F_ALONSO[8] = String.valueOf(QT_Voltas);
            }
            X++;
        }
        X = 0;
        QT_Voltas = 0;
        while (X != 4) {
            if (!S_VETTEL[X].equals("0:00.000")) {
                QT_Voltas++;
                S_VETTEL[8] = String.valueOf(QT_Voltas);
            }
            X++;
        }

        try {
            FileWriter arq = new FileWriter("D:\\Resultado.txt");
            PrintWriter gravarArq = new PrintWriter(arq);
            gravarArq.printf("Codigo   Piloto        Posição Chegada       Total de Voltas%n");
            gravarArq.printf(F_Massa[5] +       "       " + F_Massa[4] +       "         " + F_Massa[7] +       "           " + F_Massa[8]+"%n");
            gravarArq.printf(R_BARRICHELLO[5] + "       " + R_BARRICHELLO[4] + "   " + R_BARRICHELLO[7] + "           " + R_BARRICHELLO[8]+"%n");
            gravarArq.printf(K_RAIKKONEN[5] +   "       " + K_RAIKKONEN[4] +   "     " + K_RAIKKONEN[7] +   "           " + K_RAIKKONEN[8]+"%n");
            gravarArq.printf(M_WEBBER[5] +      "       " + M_WEBBER[4] +      "        " + M_WEBBER[7] +      "           " + M_WEBBER[8]+"%n");
            gravarArq.printf(F_ALONSO[5] +      "       " + F_ALONSO[4] +      "        " + F_ALONSO[7] +      "           " + F_ALONSO[8]+"%n");
            gravarArq.printf(S_VETTEL[5] +      "       " + S_VETTEL[4] +      "        " + S_VETTEL[7] +      "           " + S_VETTEL[8]+"%n");
            arq.close();
        } catch (IOException Iex) {
            System.out.println(Iex);
        }

    }
}
